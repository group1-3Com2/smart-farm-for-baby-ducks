from std_lib import *
from time import strftime, gmtime
import cv2 as cv
import turningLED as led
import servoControl as servo
import threading
import datetime

def menu():
    print("1. click 's' for feeding")
    print("2. click 'l' for open light")

cap = cv.VideoCapture(0)
servo.attach(10) # GPIO10
led.attach(17)

_isServoOpen = False
_isLightON = False

menu()

try:
    while True:
        _, frame = cap.read()
        cv.imshow('Camera', frame)

        key = cv.waitKey(1) 
        if key & 0xff == ord('s'):
            threading.Thread(target=servo.open, args=(_isServoOpen,)).start()
            _isServoOpen = not _isServoOpen
        if key & 0xff == ord('l'):
            threading.Thread(target=led.lightON, args=(_isLightON,)).start()
            _isLightON = not _isLightON
        if key & 0xff == ord('q'):
            print('Exiting...')
            break

        current_time = strftime("%H:%M:%S", gmtime())
        print(f"Currently: {current_time}",end="       \r")

except KeyboardInterrupt:
    pass

finally:
    GPIO.cleanup()
    servo.pwm.stop()
    cap.release()
    cv.destroyAllWindows()


