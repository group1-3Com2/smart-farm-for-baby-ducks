from std_lib import *

pwm = 0

def attach(SERVO_PIN):
    global pwm
    
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(SERVO_PIN, GPIO.OUT)
    pwm = GPIO.PWM(SERVO_PIN, 50)
    pwm.start(0)
    pwm.ChangeDutyCycle(7.5) # 90

def open(status):
    global pwm

    if status == True:
        pwm.ChangeDutyCycle(12.5) # 180
    else:
        pwm.ChangeDutyCycle(7.5) # 90

    sleep(2)

