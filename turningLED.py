from std_lib import *

LED_PIN = 0

def attach(pin):
    global LED_PIN
    LED_PIN = pin
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(LED_PIN, GPIO.LOW)

def lightON(status):
    global LED_PIN
    pin_status = GPIO.HIGH if status else GPIO.LOW
    GPIO.output(LED_PIN, pin_status)
    
